��          L      |       �      �   |   �   t   /  "   �     �  �  �     s  �   |  �     &   �  	   �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (https://app.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 PipeWire El servidor de audio PipeWire NO está actualmente en el inicio de antiX y NO se iniciará automáticamente la próxima vez que reinicie antiX El servidor de audio PipeWire ESTÁ actualmente en el inicio de antiX y se INICIARÁ automáticamente la próxima vez que reinicie antiX. Parece que PipeWire no está instalado Reiniciar 