��          L      |       �      �   |   �   t   /  "   �     �  �  �     P  �   Y  �   �  '   �     �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Kempelen <antixhu@netengine.hu>, 2023
Language-Team: Hungarian (https://app.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire A PipeWire hangszolgáltatás jelenleg NINCS az antiX indítási folyamatában, és NEM FOG automatikusan elindulni az antiX következő újraindításakor A PipeWire hangszolgáltatás JELENLEG az antiX indítási folyamatának része, és EL FOG automatikusan indulni az antiX következő újraindításakor Úgy tűnik a PipeWire nincs telepítve Újraindítás 