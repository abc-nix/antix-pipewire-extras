��          L      |       �      �   |   �   t   /  "   �     �  ~  �     M  �   V  �   �  (   b     �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://app.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire El servidor d'àudio PipeWire ara NO ESTÀ a l'arrencada d'antiX i NO S'INICIARÀ automàticament la pròxima vegada que arrenqueu antiX El servidor d'àudio PipeWire ara ESTÀ a l'arrencada d'antiX i S'INICIARÀ automàticament la pròxima vegada que arrenqueu antiX Sembla que PipeWire no està instal·lat Reinicia 