# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-08-01 17:52+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: toggle_pipewire:7
msgid "Reboot"
msgstr ""

#: toggle_pipewire:20
msgid ""
"PipeWire audio server IS currently on the antiX startup and WILL start "
"automatically the next time you restart antiX"
msgstr ""

#: toggle_pipewire:21
msgid "PipeWire"
msgstr ""

#: toggle_pipewire:52
msgid ""
"PipeWire audio server IS NOT currently on the antiX startup and WILL NOT "
"start automatically the next time you restart antiX"
msgstr ""

#: toggle_pipewire:78
msgid "PipeWire seems to not be installed"
msgstr ""
